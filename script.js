//Робота з JSON:
//При парсингу JSON рядків можуть виникати помилки, якщо рядок не є дійсним JSON.

//const jsonString = '{"name": "John", "age": 30}';

//try {
//    const user = JSON.parse(jsonString);
//    console.log(user.name); // Виведе: John
//} catch (error) {
//   console.log("Помилка парсингу JSON:", error.message);
//}
//Доступ до неіснуючої властивості об'єкта:
//Спроба доступу до властивості, яка не існує, може викликати помилку.

//const user = { name: "John" };

//try {
//    console.log(user.age.toString()); // Це викличе помилку, оскільки age є undefined
//} catch (error) {
 //   console.log("Помилка доступу до властивості:", error.message);
//}


const books = [
    { 
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70 
    }, 
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    }, 
    { 
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    }, 
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

function isValidBook(book) {
    const requiredFields = ['author', 'name', 'price'];
    for (const field of requiredFields) {
        if (!book.hasOwnProperty(field)) {
            console.error(`Об'єкт некоректний: відсутня властивість "${field}"`, book);
            return false;
        }
    }
    return true;
}

function createBookList(books) {
    const ul = document.createElement('ul');
    for (const book of books) {
        if (isValidBook(book)) {
            const li = document.createElement('li');
            li.textContent = `${book.author} - ${book.name} - ${book.price} грн`;
            ul.appendChild(li);
        }
    }
    return ul;
}

document.addEventListener('DOMContentLoaded', () => {
    const root = document.getElementById('root');
    const bookList = createBookList(books);
    root.appendChild(bookList);
});